import 'jquery.global.js';
import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

import Swiper from 'swiper/bundle';

import mainPage from 'index.page';

import autoComplete from "@tarekraafat/autocomplete.js";




let app = {


  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function () {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {
      const test1 = document.querySelector('.sorting-grid-js');

      const test2 = document.querySelector('.sorting-list-js');
      const contentList = document.querySelector('.content-list')


      if (test1 && test2) {
        test1.addEventListener('click', () => {
          test1.classList.add('active')
          test2.classList.remove('active')
          contentList.classList.add('grid-view')
        })

        test2.addEventListener('click', () => {
          test1.classList.remove('active')
          test2.classList.add('active')
          contentList.classList.remove('grid-view')
        })
      }

      let interestingSlider = document.querySelector('.interesting-slider .swiper-container')


      if (interestingSlider) {
        var swiper = new Swiper(interestingSlider, {
          slidesPerView: 3,
          spaceBetween: 20,
          loop: true,
          navigation: {
            nextEl: '.interesting-slider .swiper-button-next',
            prevEl: '.interesting-slider .swiper-button-prev',
          },
          breakpoints: {
            320: {
              slidesPerView: 'auto',
            },
            480: {
              slidesPerView: 'auto',
            },
            768: {
              slidesPerView: 'auto',
            },
            1280: {
              slidesPerView: 3,
              spaceBetween: 20,
            },
          }

        });
      }


      let headerSearch = new autoComplete({
        selector: "#findInput",
        placeHolder: "Поиск по сайту...",
        data: {
          src: ["Как достичь карьерного роста?", "История о том как перезагрузить карьеру", "Как достичь карьерного роста?"],

        },

        resultItem: {
          highlight: {
            render: true
          },

          element: "a",
          content: (data, element) => {


            element.addEventListener('click', () => {
              let pathElement = element.id
              // element.setAttribute("href", '/' + pathElement);
              element.setAttribute("href", '/search.html');
            });
          },
        },

        navigation: (list) => {

        },

        trigger: {
          event: ["input", "focus"]
        },

        resultsList: {
          noResults: (list, query) => {
            const message = document.createElement("div");
            message.setAttribute("class", "no_result");
            message.innerHTML = `<span>Ничего не найдено по запросу "${query}"</span>`;
            list.appendChild(message);
          },
        },

        onSelection: (feedback) => {
          // let pathFind = item.event.path[0].id;
          // console.log(pathFind);


        },

      });


      const menu = $('.header-main');
      const menuBtn = $('.js-show-btn');

      menuBtn.on('click', function (e) {
        e.stopPropagation();
        if ($(menu).hasClass('header-main--active')) {
          closeMenu();
        } else {
          openMenu();
        }
      });

      document.onclick = function (e) {
        if (e.target.className != 'header-nav-wrap') {
          menu.removeClass('header-main--active');
          menuBtn.removeClass('mobile-menu-btn--active');
          clearAllBodyScrollLocks();
        };
      };

      function openMenu() {
        $('.header-main').addClass('header-main--active');
        disableBodyScroll(menu);
        menuBtn.addClass('mobile-menu-btn--active');
      }

      function closeMenu() {
        $('.header-main').removeClass('header-main--active');
        clearAllBodyScrollLocks();
        menuBtn.removeClass('mobile-menu-btn--active');
      }

      const showMoreBtn = document.querySelector('.show-more-main');
      const listMain = document.querySelectorAll('.find-story-item');

      const showMoreFilter = document.querySelector('.show-more-filter');
      const listFilter = document.querySelectorAll('.filter-action-item');

      function showInfo(btn, list) {
        btn.addEventListener('click', (e) => {
          e.preventDefault();

          list.forEach(item => {
            if (item.classList.contains('mobile')) {
              item.classList.remove('mobile')
            }
          })

          btn.classList.add('hide')
        })
      }

      if (showMoreBtn) {
        showInfo(showMoreBtn, listMain)
      }

      if (showMoreFilter) {
        showInfo(showMoreFilter, listFilter)
      }


      const $input = document.querySelectorAll('.input');
      $input.forEach(item => {
        item.addEventListener('change', () => {
          if (item.value !== '') {
            item.closest('.input-item').classList.add('active')
          }
        })
      })

    });




    app.window.on('load', () => {


    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();