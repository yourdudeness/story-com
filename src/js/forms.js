import $ from 'jquery';
import Inputmask from 'inputmask';
import fancybox from '@fancyapps/fancybox';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';

let forms = {

  init: function () {
    forms.app = this;

    this.document.ready(() => {
      forms.initMask();
    });
  },

  initMask() {
    let selector = document.querySelectorAll('.mask-validate');
    Inputmask({
      mask: '+7 (999) 999 99 99',
      showMaskOnHover: false,
    }).mask(selector);



    $('[data-fancybox]').fancybox({
      toolbar: false,
      smallBtn: true,
      touch: false,
      autoFocus: false,

      afterLoad: function () {
        var fancyboxSlide = document.querySelectorAll(".fancybox-slide");
        fancyboxSlide.forEach(function (element) {
          // scrollLock.disablePageScroll(element);
          disableBodyScroll(element);

        });
      },
      beforeClose: function () {
        if ($('.fancybox-slide').length == 1) {
          // scrollLock.enablePageScroll();
          clearAllBodyScrollLocks();
        }
      },
    });

    const presentForm = $('.js-main-form');

    var url = $(this).attr('action');

    presentForm.on('submit', function (e) {
      e.preventDefault();
      const _form = $(this);
      const url = _form.attr('action');
      const thnxModal = '#success';
      let data = _form.serialize();

      $.ajax(url, {
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
        },
        method: 'post',
        data: data,
        success: function (result) {
          if (result.status == 'success') {
            $.fancybox.close();
            $.fancybox.open({
              toolbar: false,
              smallBtn: true,
              touch: false,
              autoFocus: false,
              src: thnxModal,
              opts: {
                afterLoad: function () {
                  let fancyboxSlide = document.querySelectorAll('.fancybox-slide');

                  fancyboxSlide.forEach(function (element) {
                    disableBodyScroll(element);
                  });
                },
                beforeClose: function () {
                  clearAllBodyScrollLocks();
                },
              },
            }, {
              autoFocus: false,
              touch: false,
            });
          } else {
            console.log('error');
          }
        }

      });

      // _form.trigger('reset');
    });



  },

};

export default forms;